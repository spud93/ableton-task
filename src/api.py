from src.definitions import User, UserAlreadyExistsError, UserNotFoundError, UserLockedOutError, PasswordWrongError,\
    UserLoggedOutError, UserAlreadyConfirmedError
from src.config import BASE_URL
import datetime
from .definitions import InputValidator


class Api(object):

    def __init__(self):
        self.inputValidator = InputValidator()

    @staticmethod
    def check_user_exists(username):
        return User.get_user_details_by_username(username) is not None

    @staticmethod
    def _strip_strings(*args):
        result = []
        for string in args:
            result.append(string.strip())
        return result

    def validate(self, **kwargs):
        for key, val in kwargs.items():
            val_fn = getattr(self.inputValidator, key, None)
            if val_fn:
                val_fn(val)

    def register_user(self, username, email, password):
        username, email, password = self._strip_strings(username, email, password)
        self.validate(username=username, email=email, password=password)

        if not self.check_user_exists(username):
            usr = User(username, new=True, raw_password=password, email=email)
            usr.write()
            self._send_confirmation_email(usr.user_id)
            return usr
        else:
            raise UserAlreadyExistsError

    def _send_confirmation_email(self, user_id):
        link = self._generate_confirmation_link(user_id)
        # send link

    def _generate_confirmation_link(self, user_id):
        return "{base}/confirmation/{user_id}".format(base=BASE_URL, user_id=user_id)

    def change_confirmed_status(self, user_id):
        usr = User.from_user_id(user_id)
        if not usr:
            raise UserNotFoundError
        if not usr.confirmed:
            usr.confirmed = True
            usr.write()
            return usr
        else:
            raise UserAlreadyConfirmedError

    @staticmethod
    def _update_user_lockout_time(usr):
        usr.failed_login_attempts += 1
        usr.third_failed_login = datetime.datetime.utcnow()
        usr.write()

    def _check_if_locked_out(self, usr):
        time_limit = datetime.datetime.utcnow() - datetime.timedelta(minutes=5)
        if usr.third_failed_login:
            if time_limit > usr.third_failed_login:
                usr.failed_login_attempts = 0
                usr.third_failed_login = None
                usr.write()
            else:
                self._update_user_lockout_time(usr)
                raise UserLockedOutError

    def attempt_login(self, username, password):

        try:
            usr = User(username)
        except UserNotFoundError:
            raise

        self._check_if_locked_out(usr)

        successful = usr.check_password(password)
        if successful:
            usr.logged_in = True
            usr.write()
            return usr
        else:
            if usr.failed_login_attempts < 2:
                usr.failed_login_attempts += 1
                usr.write()
            else:
                self._update_user_lockout_time(usr)
            raise PasswordWrongError

    @staticmethod
    def log_user_out(user_id):
        try:
            usr = User.from_user_id(int(user_id))
        except UserNotFoundError:
            raise
        if usr.logged_in:
            usr.logged_in = False
            usr.write()
            return usr
        else:
            raise UserLoggedOutError

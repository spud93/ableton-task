from hashlib import sha1
from src.db_operations.db_connector import DBConnection
from src.db_operations.sql_excerpts import create_user, update_user, select_user, select_user_by_user_id, delete_user
from .errors import UserAlreadyExistsError, PasswordNotSetError, PasswordAlreadyExistsError, UserNotFoundError,\
    EmailNotSetError, EmailAlreadyExistsError


class User(object):

    user_id = None
    _password = None
    email = None
    confirmed = False
    failed_login_attempts = 0
    third_failed_login = None
    logged_in = False

    def __init__(self, username, new=False, raw_password=None, email=None):
        self.db = DBConnection()

        self.username = username
        self.new = new

        # get details if exist
        user_details = self.get_user_details_by_username(username)

        # set from existing
        if user_details:
            if new:
                raise UserAlreadyExistsError
            self._set_from_user_details(user_details)
        elif not user_details and not new:
            raise UserNotFoundError

        # password setting
        if new and raw_password:
            self.password = raw_password
        elif new and not raw_password:
            raise PasswordNotSetError
        elif not new and raw_password:
            raise PasswordAlreadyExistsError

        # email setting
        if new and email:
            self.email = email
        elif new and not email:
            raise EmailNotSetError
        elif not new and email:
            raise EmailAlreadyExistsError

    def _set_from_user_details(self, user_details):
        user_details['_password'] = user_details.pop('password')
        self.__dict__.update(user_details)

    @classmethod
    def from_user_id(cls, user_id):
        user_details = cls.get_user_details_by_user_id(user_id)
        if user_details and 'username' in user_details:
            user = cls(user_details['username'], new=False)
            user._set_from_user_details(user_details)
            return user
        else:
            raise UserNotFoundError("User not found.")

    @staticmethod
    def _hash_password(password):
        if password:
            hasher = sha1()
            hasher.update(password.encode('utf-8'))
            return hasher.hexdigest()

    def check_password(self, raw_password_to_check):
        input_password = self._hash_password(raw_password_to_check)
        return input_password == self.password

    @property
    def password(self):
        return self._password

    @password.setter
    def password(self, password):
        self._password = self._hash_password(password)

    @staticmethod
    def get_user_details_by_user_id(user_id):
        db = DBConnection()
        with db as cursor:
            cursor.execute(select_user_by_user_id, (user_id,))
            return cursor.fetchone()

    @staticmethod
    def get_user_details_by_username(username):
        db = DBConnection()
        with db as cursor:
            cursor.execute(select_user, (username,))
            return cursor.fetchone()

    def user_exists(self):
        return self.user_id is not None

    def write(self):
        if self.new:
            with self.db as cursor:
                if self.password:
                    cursor.execute(create_user, (self.username,
                                                 self.password,
                                                 self.email,
                                                 self.confirmed,
                                                 self.failed_login_attempts,
                                                 self.third_failed_login,
                                                 self.logged_in
                                                 )
                                   )
                else:
                    raise PasswordNotSetError("Password not set.")
            self.user_id = cursor.lastrowid
            if self.user_id:
                self.new = False
        else:
            with self.db as cursor:
                cursor.execute(update_user, (self.username,
                                             self.password,
                                             self.email,
                                             self.confirmed,
                                             self.failed_login_attempts,
                                             self.third_failed_login,
                                             self.logged_in,
                                             self.user_id
                                             )
                               )

    def delete_user(self):
        with self.db as cursor:
            cursor.execute(delete_user, (self.user_id,))
            self.new = True
            self.user_id = None

    def to_dict(self):
        return {
                "username": self.username,
                "email": self.email,
                "confirmed": self.confirmed,
                "logged_in": self.logged_in,
                "user_id": self.user_id
               }

    def reload(self):
        user_details = self.get_user_details_by_user_id(self.user_id)
        if user_details:
            self._set_from_user_details(user_details)
            self.new = False
        return self

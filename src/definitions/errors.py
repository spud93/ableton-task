# user related errors
class UserAlreadyExistsError(Exception):
    pass


class UserNotFoundError(Exception):
    pass


class UserAlreadyConfirmedError(Exception):
    pass


class PasswordWrongError(Exception):
    pass


class PasswordNotSetError(Exception):
    pass


class PasswordAlreadyExistsError(Exception):
    pass


class EmailNotSetError(Exception):
    pass


class EmailAlreadyExistsError(Exception):
    pass


class UserLockedOutError(Exception):
    pass


class UserLoggedOutError(Exception):
    pass


# validation errors

class ValidationError(Exception):
    pass


class InvalidUsernameError(ValidationError):
    pass


class InvalidPasswordError(ValidationError):
    pass


class InvalidEmailError(ValidationError):
    pass

import re
from . import InvalidUsernameError, InvalidPasswordError, InvalidEmailError


class InputValidator(object):

    def username(self, val):
        if len(val) > 30:
            raise InvalidUsernameError("should 30 characters or fewer")
        return True

    def email(self, val):
        if len(val) > 30:
            raise InvalidEmailError("should 30 characters or fewer")
        if not re.match(r"[^@]+@[^@]+\.[^@]+", val):
            raise InvalidEmailError("should follow normal email format")
        return True

    def password(self, val):
        if len(val) > 30:
            raise InvalidPasswordError("should 30 characters or fewer")
        return True

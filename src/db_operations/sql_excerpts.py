create_user_table = '''
                                CREATE TABLE IF NOT EXISTS users (
                                    user_id                 INTEGER PRIMARY KEY,
                                    username                VARCHAR(30) UNIQUE NOT NULL,
                                    password                varchar(30) NOT NULL,
                                    email                   varchar(30) UNIQUE NOT NULL,
                                    confirmed               INTEGER NOT NULL,
                                    failed_login_attempts   INTEGER NOT NULL,
                                    third_failed_login      TIMESTAMP,
                                    logged_in               INTEGER NOT NULL
                                );
                    '''

create_user = '''
                                INSERT INTO users 
                                (
                                    username,
                                    password,
                                    email,
                                    confirmed,
                                    failed_login_attempts,
                                    third_failed_login,
                                    logged_in
                                )
                                VALUES (
                                    ?,
                                    ?,
                                    ?,
                                    ?,
                                    ?,
                                    ?,
                                    ?
                                );
              '''

update_user = '''
                                UPDATE users 
                                SET username  = ?,
                                    password  = ?,
                                    email     = ?,
                                    confirmed = ?,
                                    failed_login_attempts = ?,
                                    third_failed_login = ?,
                                    logged_in = ?
                                WHERE
                                    user_id   = ?
                                ;
              '''


select_user = '''
                                SELECT * FROM users
                                WHERE username = ?
                                ;
              '''

select_user_by_user_id = '''
                                SELECT * FROM users
                                WHERE user_id = ?
                                ;
                        '''

delete_user = '''
                                DELETE FROM users
                                WHERE user_id = ?
                                ;
              '''

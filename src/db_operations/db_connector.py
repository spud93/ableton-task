import sqlite3
from src.config import DB_LOCATION
from .sql_excerpts import create_user_table


def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


class DBConnection(object):

    def __init__(self):
        self.dbconn = self._connect()
        self.dbconn.row_factory = dict_factory
        cursor = self.dbconn.cursor()
        cursor.execute(create_user_table)
        self.dbconn.commit()

    def __enter__(self):
        return self.dbconn.cursor()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.dbconn.commit()

    @staticmethod
    def _connect():
        return sqlite3.connect(DB_LOCATION, detect_types=sqlite3.PARSE_DECLTYPES)

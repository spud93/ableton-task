import os

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
BASE_URL = "http://localhost:5000"
DB_LOCATION = os.path.join(ROOT_DIR, "user.db")

from flask import Flask, request
from flask_api import status
from src.api import Api
from src.definitions import UserAlreadyExistsError, UserNotFoundError, UserLockedOutError, PasswordWrongError,\
    UserLoggedOutError, ValidationError, UserAlreadyConfirmedError


def create_app():
    user_server = Flask(__name__)
    api = Api()

    @user_server.route('/status')
    def check_status():
        return {"message": "status is good"}, status.HTTP_200_OK

    @user_server.route('/register', methods=['POST'])
    def register():
        username = request.json['username']
        email = request.json['email']
        password = request.json['password']

        try:
            usr = api.register_user(username, email, password)
            if usr.user_id:
                return {"message": "success", "user": usr.to_dict()}, status.HTTP_200_OK
            else:
                return {"message": "user not created"}, status.HTTP_500_INTERNAL_SERVER_ERROR
        except UserAlreadyExistsError:
            return {"message": "user already exists"}, status.HTTP_400_BAD_REQUEST
        except ValidationError as e:
            return {"error_type": e.__class__.__name__,
                    "message": str(e)}, status.HTTP_400_BAD_REQUEST

    @user_server.route('/confirmation/<user_id>', methods=['GET'])
    def confirm(user_id):

        try:
            usr = api.change_confirmed_status(user_id)
            return {"message": "success", "user": usr.to_dict()}, status.HTTP_200_OK
        except UserNotFoundError:
            return {"message": "user not found"}, status.HTTP_400_BAD_REQUEST
        except UserAlreadyConfirmedError:
            return {"message": "user already confirmed"}, status.HTTP_400_BAD_REQUEST

    @user_server.route('/login', methods=['POST'])
    def login():
        username = request.json['username']
        password = request.json['password']

        try:
            usr = api.attempt_login(username, password)
            return {"message": "success", "user": usr.to_dict()}, status.HTTP_200_OK
        except UserNotFoundError:
            return {"message": "user not found"}, status.HTTP_401_UNAUTHORIZED
        except UserLockedOutError:
            return {"message": "user locked out, try again in 5 mins"}, status.HTTP_401_UNAUTHORIZED
        except PasswordWrongError:
            return {"message": "password wrong"}, status.HTTP_401_UNAUTHORIZED

    @user_server.route('/logout', methods=['POST'])
    def logout():
        user_id = request.json['user_id']
        try:
            usr = api.log_user_out(user_id)
            return {"message": "success", "user": usr.to_dict()}, status.HTTP_200_OK
        except UserNotFoundError:
            return {"message": "user not found"}, status.HTTP_401_UNAUTHORIZED
        except UserLoggedOutError:
            return {"message": "user logged out already"}, status.HTTP_400_BAD_REQUEST

    return user_server


if __name__ == "__main__":
    app = create_app()
    app.run()

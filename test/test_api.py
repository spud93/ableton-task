import pytest
from unittest.mock import patch
from . import connect
from src import Api
from src.definitions import PasswordWrongError, UserNotFoundError, UserLockedOutError, UserLoggedOutError,\
    InvalidUsernameError, InvalidEmailError, InvalidPasswordError
import datetime

api = Api()
username = 'thomas'
raw_password = 'monolake'
email = "thomas@asifnoway.com"


@patch("src.db_operations.db_connector.DBConnection._connect", connect)
def test_register_user_exists():
    usr = api.register_user(username, email, raw_password)
    assert usr.user_id
    assert usr.username == username
    assert usr.email == email
    assert api.check_user_exists(username)

@patch("src.db_operations.db_connector.DBConnection._connect", connect)
def test_register_user_invalid_username():
    with pytest.raises(InvalidUsernameError):
        api.register_user(username*100, email, raw_password)


@patch("src.db_operations.db_connector.DBConnection._connect", connect)
def test_register_user_invalid_email():
    with pytest.raises(InvalidEmailError):
        api.register_user(username, username, raw_password)


@patch("src.db_operations.db_connector.DBConnection._connect", connect)
def test_register_user_invalid_password():
    with pytest.raises(InvalidPasswordError):
        api.register_user(username, email, raw_password*100)


@patch("src.db_operations.db_connector.DBConnection._connect", connect)
def test_change_confirmed_status():
    usr = api.register_user(username, email, raw_password)
    usr = api.change_confirmed_status(usr.user_id)
    assert usr.confirmed


@patch("src.db_operations.db_connector.DBConnection._connect", connect)
def test_attempt_login():
    usr1 = api.register_user(username, email, raw_password)
    usr2 = api.attempt_login(username, raw_password)
    assert usr1.user_id == usr2.user_id
    assert not usr1.logged_in
    assert usr2.logged_in


@patch("src.db_operations.db_connector.DBConnection._connect", connect)
def test_attempt_login_wrong_password():
    usr = api.register_user(username, email, raw_password)
    assert usr.user_id
    with pytest.raises(PasswordWrongError):
        api.attempt_login(username, raw_password[1:])


@patch("src.db_operations.db_connector.DBConnection._connect", connect)
def test_attempt_login_user_not_found():
    usr = api.register_user(username, email, raw_password)
    assert usr.user_id
    with pytest.raises(UserNotFoundError):
        api.attempt_login(username[1:], raw_password)


@patch("src.db_operations.db_connector.DBConnection._connect", connect)
def test_attempt_login_locked_out():
    usr = api.register_user(username, email, raw_password)
    for _ in range(3):
        try:
            api.attempt_login(username, raw_password[1:])
        except PasswordWrongError:
            pass

    usr.reload()

    assert usr.failed_login_attempts == 3
    assert isinstance(usr.third_failed_login, datetime.datetime)

    with pytest.raises(UserLockedOutError):
        api.attempt_login(username, raw_password)

    usr.reload()

    assert usr.failed_login_attempts == 4
    assert isinstance(usr.third_failed_login, datetime.datetime)

    usr.third_failed_login = datetime.datetime.utcnow() - datetime.timedelta(minutes=10)
    usr.write()

    usr = api.attempt_login(username, raw_password)
    assert usr.logged_in
    assert usr.failed_login_attempts == 0
    assert usr.third_failed_login is None


@patch("src.db_operations.db_connector.DBConnection._connect", connect)
def test_attempt_logout():
    usr = api.register_user(username, email, raw_password)
    usr.logged_in = True
    usr.write()
    api.log_user_out(usr.user_id)
    usr.reload()


@patch("src.db_operations.db_connector.DBConnection._connect", connect)
def test_attempt_logout_already_logged_out():
    usr = api.register_user(username, email, raw_password)
    usr.reload()
    assert not usr.logged_in
    with pytest.raises(UserLoggedOutError):
        api.log_user_out(usr.user_id)


@patch("src.db_operations.db_connector.DBConnection._connect", connect)
def test_attempt_logout_user_not_found():
    with pytest.raises(UserNotFoundError):
        api.log_user_out(100)


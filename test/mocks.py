import sqlite3

DB_LOC = 'file::memory:?cache=shared'
connect = lambda _: sqlite3.connect(DB_LOC, detect_types=sqlite3.PARSE_DECLTYPES, uri=True)



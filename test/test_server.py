import pytest
from src import create_app, Api
from unittest.mock import patch
from flask_api import status
from . import connect
import json
import datetime

username = 'thomas'
raw_password = 'monolake'
email = "thomas@asifnoway.com"

@pytest.fixture
def app():
    my_app = create_app()
    return my_app


@patch("src.db_operations.db_connector.DBConnection._connect", connect)
def test_status(client):
    response = client.get("/status")
    assert response.status_code == status.HTTP_200_OK
    assert json.loads(response.data) == {"message": "status is good"}


@patch("src.db_operations.db_connector.DBConnection._connect", connect)
def test_register(client):
    response = client.post("/register", json={"username": username, "email": email, "password": raw_password})
    assert response.status_code == status.HTTP_200_OK
    response_data = json.loads(response.data)
    assert response_data['message'] == 'success'
    assert response_data['user']['username'] == username
    assert response_data['user']['confirmed'] is False
    assert response_data['user']['email'] == email
    assert response_data['user']['logged_in'] is False


@patch("src.db_operations.db_connector.DBConnection._connect", connect)
def test_register_user_already_exists(client):
    api = Api()
    usr = api.register_user(username, email, raw_password)
    assert usr
    response = client.post("/register", json={"username": username, "email": email, "password": raw_password})
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    response_data = json.loads(response.data)
    assert response_data["message"] == "user already exists"


@patch("src.db_operations.db_connector.DBConnection._connect", connect)
def test_register_invalid_email(client):
    response = client.post("/register", json={"username": username, "email": username, "password": raw_password})
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    response_data = json.loads(response.data)
    assert response_data["error_type"] == "InvalidEmailError"
    assert response_data["message"] == "should follow normal email format"


@patch("src.db_operations.db_connector.DBConnection._connect", connect)
def test_confirm(client):
    api = Api()
    usr = api.register_user(username, email, raw_password)
    assert usr

    response = client.get("/confirmation/{}".format(usr.user_id))
    assert response.status_code == status.HTTP_200_OK
    response_data = json.loads(response.data)

    assert response_data['user']['confirmed'] is True


@patch("src.db_operations.db_connector.DBConnection._connect", connect)
def test_confirm_already_confirmed(client):
    api = Api()
    usr = api.register_user(username, email, raw_password)
    assert usr
    usr.confirmed = True
    usr.write()
    usr.reload()
    assert usr.confirmed

    response = client.get("/confirmation/{}".format(usr.user_id))
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    response_data = json.loads(response.data)
    assert response_data['message'] == 'user already confirmed'

@patch("src.db_operations.db_connector.DBConnection._connect", connect)
def test_confirm_user_not_found(client):
    response = client.get("/confirmation/{}".format(100))
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    response_data = json.loads(response.data)
    assert response_data['message'] == "user not found"


@patch("src.db_operations.db_connector.DBConnection._connect", connect)
def test_login(client):
    api = Api()
    usr = api.register_user(username, email, raw_password)
    assert usr
    response = client.post("/login", json={"username": username, "password": raw_password})
    assert response.status_code == status.HTTP_200_OK
    usr.reload()
    response_data = json.loads(response.data)
    assert response_data['message'] == 'success'
    assert response_data['user'] == usr.to_dict()


@patch("src.db_operations.db_connector.DBConnection._connect", connect)
def test_login_user_not_found(client):
    response = client.post("/login", json={"username": username, "password": raw_password})
    assert response.status_code == status.HTTP_401_UNAUTHORIZED
    response_data = json.loads(response.data)
    assert response_data['message'] == "user not found"


@patch("src.db_operations.db_connector.DBConnection._connect", connect)
def test_login_user_locked_out(client):
    api = Api()
    usr = api.register_user(username, email, raw_password)
    assert usr
    usr.failed_login_attempts = 3
    usr.third_failed_login = datetime.datetime.utcnow()
    usr.write()
    response = client.post("/login", json={"username": username, "password": raw_password})
    assert response.status_code == status.HTTP_401_UNAUTHORIZED
    response_data = json.loads(response.data)
    assert response_data['message'] == "user locked out, try again in 5 mins"


@patch("src.db_operations.db_connector.DBConnection._connect", connect)
def test_login_password_wrong(client):
    api = Api()
    usr = api.register_user(username, email, raw_password)
    assert usr
    response = client.post("/login", json={"username": username, "password": raw_password[1:]})
    assert response.status_code == status.HTTP_401_UNAUTHORIZED
    response_data = json.loads(response.data)
    assert response_data['message'] == "password wrong"


@patch("src.db_operations.db_connector.DBConnection._connect", connect)
def test_logout(client):
    api = Api()
    usr = api.register_user(username, email, raw_password)
    assert usr
    usr.logged_in = True
    usr.write()
    response = client.post("/logout", json={"user_id": usr.user_id})
    assert response.status_code == status.HTTP_200_OK
    usr.reload()
    response_data = json.loads(response.data)
    assert response_data['message'] == 'success'
    assert response_data['user'] == usr.to_dict()


@patch("src.db_operations.db_connector.DBConnection._connect", connect)
def test_logout_user_not_found(client):
    response = client.post("/logout", json={"user_id": 100})
    assert response.status_code == status.HTTP_401_UNAUTHORIZED
    response_data = json.loads(response.data)
    assert response_data['message'] == "user not found"


@patch("src.db_operations.db_connector.DBConnection._connect", connect)
def test_logout_user_logged_out(client):
    api = Api()
    usr = api.register_user(username, email, raw_password)
    assert usr
    usr.logged_in = False
    usr.write()
    response = client.post("/logout", json={"user_id": usr.user_id})
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    usr.reload()
    response_data = json.loads(response.data)
    assert response_data['message'] == 'user logged out already'

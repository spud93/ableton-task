import pytest
from unittest.mock import patch
from src.definitions import UserAlreadyExistsError, User
from . import connect

import hashlib

username = 'thomas'
raw_password = 'monolake'
email = "thomas@asifnoway.com"


@patch("src.db_operations.db_connector.DBConnection._connect", connect)
def test_create_new_user():

    hasher = hashlib.sha1()
    hasher.update(raw_password.encode('utf-8'))

    new_user = User(username, new=True, raw_password=raw_password, email=email)
    new_user.write()

    assert new_user.username == username
    assert new_user.email == email
    assert new_user.password == hasher.hexdigest()
    assert new_user.logged_in == False
    assert new_user.failed_login_attempts == 0
    assert not new_user.third_failed_login
    assert new_user.user_id is not None


@patch("src.db_operations.db_connector.DBConnection._connect", connect)
def test_create_duplicate():

    new_user = User(username, new=True, raw_password=raw_password, email=email)
    new_user.write()

    with pytest.raises(UserAlreadyExistsError):
        new_user2 = User(username, new=True, raw_password=raw_password, email=email)
        new_user2.write()


@patch("src.db_operations.db_connector.DBConnection._connect", connect)
def test_check_password():

    new_user = User(username, new=True, raw_password=raw_password, email=email)

    assert new_user.check_password(raw_password)


@patch("src.db_operations.db_connector.DBConnection._connect", connect)
def test_from_user_id():

    new_user = User(username, new=True, raw_password=raw_password, email=email)
    new_user.write()

    assert new_user.user_id

    user_from_id = User.from_user_id(new_user.user_id)

    assert new_user.to_dict() == user_from_id.to_dict()


@patch("src.db_operations.db_connector.DBConnection._connect", connect)
def test_user_exists():

    new_user = User(username, new=True, raw_password=raw_password, email=email)

    assert not new_user.user_id
    assert not new_user.user_exists()
    new_user.write()
    assert new_user.user_id
    assert new_user.user_exists()


@patch("src.db_operations.db_connector.DBConnection._connect", connect)
def test_delete_user():

    new_user = User(username, new=True, raw_password=raw_password, email=email)
    new_user.write()
    user_id = new_user.user_id
    new_user.delete_user()
    assert new_user.new
    assert new_user.user_id is None

    usr = User.get_user_details_by_user_id(user_id)

    assert usr is None


@patch("src.db_operations.db_connector.DBConnection._connect", connect)
def test_user_reload():

    new_user = User(username, new=True, raw_password=raw_password, email=email)
    new_user.write()

    same_user = User(username)
    same_user.confirmed = True
    same_user.write()

    assert new_user.to_dict() != same_user.to_dict()

    new_user.reload()

    assert new_user.to_dict() == same_user.to_dict()

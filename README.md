# Ableton Interview Task

## Setup

```bash
git clone git@bitbucket.org:spud93/ableton-task.git
cd ableton-task
# create virtualenv
python3 -m virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
```

## Run Server

```bash
cd ableton-task
source venv/bin/activate
# export Flask App location
export FLASK_APP=src/server.py
flask run
```

Postman files are included for simplicity when testing REST API.

## Run Tests
```bash
cd ableton-task
source venv/bin/activate
pytest tests/
```
